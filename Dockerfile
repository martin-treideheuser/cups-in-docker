FROM debian:wheezy

MAINTAINER Martin Treide-Heuser "martin.treideheuser@gmail.com"

# Initial setup
RUN apt-get update
RUN apt-get -yq install locales
RUN echo "LANG=en_US.UTF-8" > /etc/default/locale
RUN dpkg-reconfigure locales
RUN update-locale en_US.UTF-8
RUN locale-gen --purge --no-archive

# Install stuff
RUN apt-get update && apt-get install vim cups cups-pdf whois -y && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Setup stuff
RUN echo "syntax on" > /root/.vimrc


# Disbale some cups backend that are unusable within a container
RUN mv /usr/lib/cups/backend/parallel /usr/lib/cups/backend-available/ &&\
    mv /usr/lib/cups/backend/serial /usr/lib/cups/backend-available/ &&\
    mv /usr/lib/cups/backend/usb /usr/lib/cups/backend-available/ &&\
    mv /usr/lib/cups/backend/dnssd /usr/lib/cups/backend-available/ &&\
    mv /usr/lib/cups/backend/mdns /usr/lib/cups/backend-available/ &&\
    mv /usr/lib/cups/backend/smb /usr/lib/cups/backend-available/ &&\
    mv /usr/lib/cups/backend/cups-pdf /usr/lib/cups/backend-available/ 

ADD etc-cups /etc/cups
RUN mkdir -p /etc/cups/ssl
VOLUME /etc/cups/
VOLUME /var/log/cups
VOLUME /var/spool/cups
VOLUME /var/cache/cups

ADD etc-pam.d-cups /etc/pam.d/cups

EXPOSE 631 515 161

ADD start_cups.sh /root/start_cups.sh
RUN chmod +x /root/start_cups.sh
CMD ["/root/start_cups.sh"]


