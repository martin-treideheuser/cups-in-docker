This is taken from https://github.com/ticosax/cups-in-docker
and modified for a very quick deployment of a cups server.

h1. Cups printing service

h2. General information

h3. Machine information
* Virtual machine
* cups.xy.local
* 192.168.xx.xx

h3. Accessibility
* ssh user@cups.xx.local ('sudo -i' recommended)
* https://cups.xx.local:631

h3. How to install the printers on a linux client:

After you installed "cups-client" and "cups-bsd", you shouldn't need more than:
<pre>
(sudo) echo "ServerName cups.pb.local" > /etc/cups/client.conf
</pre>

Test with "lpstat -a" command.

They're currently some issued but I try to go around by re-configuring the server
to prevent client needed installations.

h2. Detailed sysadmin instructions

h3. Administration instructions

h5. Entering the container (docker > 1.3)

 docker exec -ti cups bash

Check with ps aux which should display this

<pre>
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.5  88048  5760 ?        Ss   10:16   0:00 /usr/sbin/cupsd -f
root        10  0.3  0.2  17840  2892 ?        S    10:33   0:00 bash
root        18  0.0  0.1  15324  1848 ?        R+   10:33   0:00 ps aux
</pre>


h3. Build / Restore instructions

h3. Manual start / stop instructions

 docker stop cups

 docker start cups

h3. Re-create/start a new container instructions / Just throw away design in case of failure

 docker run --name cups -h cups -d -e CUPS_USER_ADMIN=admin -e CUPS_USER_PASSWORD=secr3t -p 161/161/udp -p 515/515/tcp -p 631:631/tcp cups-in-docker

h3. Restore virtual machine instructions

* Prepare a host
* Do:

<pre>
 apt-get install docker.io
</pre>

* Go on with Build / Restore instructions (Just deploy and enjoy ;-) )

h3. Helper files:

* /etc/rc.local

<pre>
sleep 10
/usr/bin/logger "/etc/rc.local - Starting existing docker conatainers"
for i in `docker ps -aq`; do docker start $i; sleep 1; done
</pre>


https://gitlab.com/martin-treideheuser/cups-in-docker
